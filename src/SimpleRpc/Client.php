<?php

namespace SimpleRpc;

class Client {
    /**
     * The API "Client ID" used as the public identifier for the token 
     */
    private $key;

    /**
     * The API "Secret Key" used to sign the requests
     */
    private $secret;

    /**
     * The API server to communicate with
     */
    private $client;

    function __construct($key, $secret, $server)
    {
        $this->key = $key;
        $this->secret = $secret;
        $this->client = new \GuzzleHttp\Client([ "base_uri" => $server ]);
    }

    private function auth() {
        $verify = (int)date("U");
        $token = hash("sha256", $this->secret.$verify);
        return [
            "key" => $this->key,
            "token" => $token,
            "verify" => $verify
        ];
    }

    private function generateQuery($body) {
        $query = "";
        $index = 0;

        foreach($body as $key => $val) {
            if (is_array($val)) $query .= urlencode($key)."=".urlencode(json_encode($val));
            else $query .= urlencode($key)."=".urlencode($val);

            $index++;
            if ($index !== count($body)) $query .= "&";            
        }

        return $query;
    }

    function request($method, $args = []) {
        $args = array_merge($args, $this->auth());
        
        $response = $this->client->request("GET", $method.'.php?'.$this->generateQuery($args));
        return json_decode($response->getBody(), true);
    }
}