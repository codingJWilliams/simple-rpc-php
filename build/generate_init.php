<?php 

$out = "<?php\n\nrequire __DIR__ . '/vendor/autoload.php';\n";

function scan($dir, $out) {
    $base = __DIR__.'/../src';

    foreach(scandir($base.$dir) as $f) {
        //echo " - $f\n";
        if (in_array($f, [ ".", ".." ])) continue;
        if (is_dir($base.$dir.'/'.$f)) $out = scan($dir.'/'.$f, $out."\n");
        else $out .= "require __DIR__ . '/src".$dir.'/'.$f."';\n";
    }
    return $out;
}

$out = scan('', $out);
file_put_contents(__DIR__.'/../init.php', $out);